#!/usr/bin/python

import os
import csv
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
from operator import itemgetter
import re
import datetime

def readable_timestamp(timestamp):
	return datetime.datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')

def get_iperf_points(outputfolder):
	trim = 5
	points = []

	all_rates = {}

	files = os.listdir(outputfolder)
	for filename in files:
		if(filename.startswith("iperf_from")):
			iperf_file = open(outputfolder + "/" + filename, 'rb')
			for line in iperf_file:
				parts = line.strip().split(",")
				if(len(parts) == 9):
					interval = parts[7]
					bw = float(parts[-1])
					if filename not in all_rates:
						all_rates[filename] = []
					all_rates[filename].append((interval, bw))
				

		
	for filename,samples in all_rates.iteritems():
		rates = [float(x[1]) for x in samples]
		if(len(rates) < trim*2):
			print "ERROR: bandwidth sample must be larger than trice the trim (" + str(trim*2) + ")"
		rates = rates[trim:len(rates) - trim]

		mean = sum(rates) / float(len(rates))
		# print iface + " " + str(mean)
		points.append(mean)


	points.sort()
	maxVal = points[-1]
	points = [float(x) / float(maxVal) for x in points]
	print "IPERF POINTS: " + str(points)
	print "num points: " + str(len(points))
	return points


def get_bwm_points(outputfolder, iface_regex):
	trim = 5
	points = []

	all_rates = {}


	''' unix timestamp;iface_name;bytes_out/s;bytes_in/s;bytes_total/s;
		bytes_in;bytes_out;packets_out/s;packets_in/s;packets_total/s;
		packets_in;packets_out;errors_out/s;errors_in/s;errors_in;errors_out;errors_total
	'''
	file = open(outputfolder + "/bwm.txt", "rb")
	for line in file:
		parts = line.strip().split(",")
		if(len(parts) == 16):
			timestamp = parts[0]
			iface = parts[1]
			in_rate = parts[3]

			if(re.match(iface_regex, iface)):
				parts = iface.split("_")
				switch_no = int(parts[0])
				if(switch_no <= 16): # THIS IS A TOTAL HACK TO ELIMINATE INNER JELLYFISH SWITCHES, WILL FAIL WITH MORE THAN 16 HOSTS
					if iface not in all_rates:
						all_rates[iface] = []
				
					all_rates[iface].append((readable_timestamp(timestamp), in_rate))

		else:
			print "Incomplete line: " + line

	print "Rates: " + str(all_rates.keys())

	for iface,samples in all_rates.iteritems():
		rates = [float(x[1]) for x in samples]
		if(len(rates) < trim*3):
			print "ERROR: bandwidth sample must be larger than trice the trim (" + str(trim*2) + ")"
		rates = rates[trim + 5:len(rates) - trim]

		mean = sum(rates) / float(len(rates))
		# print iface + " " + str(mean)
		points.append(mean)


	points.sort()
	maxVal = points[-1]
	points = [float(x) / float(maxVal) for x in points]
	print "BWM POINTS: " + str(points)
	print "num points: " + str(len(points))
	return points

def get_avg_iperf_points(outputfolder, iface_regex):
	sums = None
	run = 0
	while(True):
		output_dir = outputfolder +"_"+ str(run)
		if not os.path.exists(output_dir):
			print "Could not find " + output_dir
			break

		points = get_iperf_points(output_dir)
		if(sums == None):
			sums = points
		else:
			for i in range(len(points)):
				sums[i] += points[i]
		# plt.plot(range(len(points)), points, label="Run # " + str(run+1))
		run += 1

	# print "sums: " + str(sums)
	avgs = [x / run for x in sums]
	return avgs

def get_avg_bwm_points(outputfolder, iface_regex):
	sums = None
	run = 0
	while(True):
		output_dir = outputfolder +"_"+ str(run)
		if not os.path.exists(output_dir):
			print "Could not find " + output_dir
			break

		bwm_points = get_bwm_points(output_dir, iface_regex)
		if(sums == None):
			sums = bwm_points
		else:
			for i in range(len(bwm_points)):
				sums[i] += bwm_points[i]
		# plt.plot(range(len(bwm_points)), bwm_points, label="Run # " + str(run+1))
		run += 1

	# print "sums: " + str(sums)
	avgs = [x / run for x in sums]
	return avgs

def main():

	mptcp_fatree_output = "output_mptcp_fattree"
	# iperf_points = get_iperf_points(mptcp_fatree_output)
	# plt.plot(range(len(iperf_points)), iperf_points, label="iperf")

	# ft_points = get_bwm_points("output_mptcp_ft_0", iface_regex="[0-3]_[0-1]_1-eth[24]")


	# ft_points = get_iperf_points("output_mptcp_ft_0")
	# plt.plot(range(len(ft_points)), ft_points, label="Fat-Tree")


 #    jf_points = get_iperf_points("output_mptcp_jf_0")
	# plt.plot(range(len(ft_points)), jf_points, label="Jellyfish")

	# bwm_single_points = get_bwm_points("_output_single_fattree_1");
	# plt.plot(range(len(bwm_single_points)), bwm_single_points, label="Single Path TCP")

	# jf_points = get_bwm_points("output_mptcp_jf_0", iface_regex="[0-9]+_0-eth1")
	# plt.plot(range(len(jf_points)), jf_points, label="Jellyfish")

	jf_avgs = get_avg_bwm_points("output_mptcp_jf", iface_regex="[0-9]+_0-eth1")
	plt.plot(range(len(jf_avgs)), jf_avgs, label="Jellyfish (avg)")


	ft_avgs = get_avg_bwm_points("output_mptcp_ft", iface_regex="[0-3]_[0-1]_1-eth[24]")
	plt.plot(range(len(ft_avgs)), ft_avgs, label="Fat-Tree (avg)")

	plt.xlabel("Rank of Flow")
	plt.ylabel("Normalized Throughput")
	plt.title("Fairness of Jellyfish vs Fat-Tree")
	plt.legend(loc=4)
	plt.ylim((0,1))
	plt.savefig("rank.png")



if __name__ == '__main__':
	main()
