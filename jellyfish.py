#!/usr/bin/python

from time import sleep, time
# import time
from datetime import datetime
from multiprocessing import Process
from subprocess import Popen
import shlex
from mininet.node import RemoteController
from ripl.ripl.dctopo import FatTreeTopo, JellyFishTopo
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.util import custom, dumpNetConnections
import sys
import os
from util.monitor import monitor_devs_ng
from random import choice, seed, randint, shuffle
import logging

def run_iperf(net, iperf, outputfolder, seconds, sender, receiver):
	receiverNode = net.getNodeByName(receiver)
	senderNode = net.getNodeByName(sender)
	senderNode.sendCmd("%s -c %s -t %d -i 1  > %s/iperf_from_%s_to_%s.txt" % (iperf, receiverNode.IP(), seconds, outputfolder, sender, receiver))

def run_experiment(net=None, topo=None, flows=1, outputfolder="output_default"):

	if not os.path.exists(outputfolder):
		os.makedirs(outputfolder)

	#Settings
	seconds = 60
	port = 5001
	iperf = "~/iperf-patched/src/iperf"
	# iperf = "iperf"
	print "Flows per host: " + str(flows)
	print "Flow duration: " + str(seconds)
	print "Iperf: " + iperf

	monitor = Process(target=monitor_devs_ng, args=('%s/bwm.txt' % outputfolder, 1.0))
	monitor.start()

	#Get all the hosts
	hosts = topo.hosts()

	print "Starting receivers"
	#Start iperf servers
	for host in hosts:
		h = net.getNodeByName(host)
		h.cmd("%s -s > %s/iperf_receiver_%s.txt &" % (iperf, outputfolder, h.name))
		h.waiting = False
		# print "Started receiver: " + host

	

	start = time()

	receivers = list(hosts)


	# Keep shuffling traffic matrix until nobody sends a flow to themself
	valid = False
	while(not valid):
		shuffle(receivers)
		valid = True
		for i in range(len(hosts)):
			if hosts[i] == receivers[i]:
				valid = False

	numFlows = 0
	for i in range(len(hosts)):
		sender = hosts[i]
		receiver = receivers[i]
		print "Starting from: " + sender + " to: " + receiver 
		s = net.getNodeByName(sender)
		r = net.getNodeByName(receiver)
		s.sendCmd("%s -c %s -t %d -i 1  -yc > %s/iperf_from_%s_to_%s.txt" % (iperf, r.IP(), seconds, outputfolder, s.name, r.name))
		# s.sendCmd("%s -c %s -p %s -t %d -i 1  -yc > %s/iperf_from_%s.txt" % (iperf, r.IP(), port, seconds, outputfolder, s.name))

		s.waiting = False
		numFlows += 1




	print "Waiting for " + str(numFlows) + " flows to complete"


	# run_iperf(net, iperf, outputfolder, seconds, "1_0_2", "0_0_2")
	# run_iperf(net, iperf, outputfolder, seconds, "1_0_3", "0_0_3")

	# seconds_left = seconds;
	# while(seconds_left > 0):
	# 	print str(seconds_left) + " seconds left"
	# 	sleep(1)
	# 	seconds_left -= 1
	# for host in hosts:
	# 	# print "Wait output: " + host
	# 	h = net.getNodeByName(host)
	# 	# h.waitOutput()
	# 	h.monitor(seconds * 1000)


	# sleep(seconds)


	for host in hosts:
		h = net.getNodeByName(host)
		h.sendCmd('kill %iperf')

	sleep(seconds + 10)


	end = time()
	print "All flows took: " + str(end - start) + " seconds to complete"

	monitor.terminate()
	os.system("killall -9 bwm-ng")

	os.system('killall -9 iperf' )

	print "Finished experiment"


def main(topo_name="jf"):
	# logger = logging.getLogger('jellyfish')
	# logger.setLevel(logging.WARNING)

	print "Creating topology"
	n = 20
	k = 4
	h = 16

	seed(2)

	n_experiments = 8
	for exp_no in range(n_experiments):
		print "Running experiment # " + str(exp_no)
		#topo_name = "jf"

		use_seed = randint(0,1000)
	    
		if topo_name == "ft": 
		    topo = FatTreeTopo(k=k)
		elif topo_name == "jf":
		    topo = JellyFishTopo(n=n, k=k, h=h, use_seed=use_seed)
		else:
		    sys.exit(1)
		# host = custom(CPULimitedHost, cpu=.5)  # 15% of system bandwidth
		# link = custom(TCLink, delay='.05ms', max_queue_size=200)

		host = custom(CPULimitedHost, cpu=.0625)  # 15% of system bandwidth

		link = custom(TCLink, bw=15, delay='.05ms', max_queue_size=200)


		net = Mininet(topo=topo, host=host, link=link, controller=RemoteController,autoSetMacs=True)
		# net = Mininet(topo=topo, controller=RemoteController,autoSetMacs=True)

		print "Starting mininet"
		net.start()

		print "*** Dumping network connections:"

		dumpNetConnections(net)


		if(topo_name == "ft"):
			raw_args = shlex.split("/home/ubuntu/pox/pox.py riplpox.riplpox --topo=%s,%s --routing=random" % (topo_name, k))
		else:
			raw_args = shlex.split("/home/ubuntu/pox/pox.py riplpox.riplpox --topo=%s,%s,%s,%s,%s --routing=jelly" % (topo_name, n,k,h,use_seed))
		
		print "Starting RiplPox"
		p = Popen(raw_args)

		print "********************************************************"
		print "*******************STARTED RIPLPOX**********************"

		#This sleep 10 is to give the riplpox controller time to start up before you start sending.  Seems to work well.
		sleep(25)

		print "Finished waiting for RiplPox"
		#Test connectivity and start senders here after sleeping
		# net.pingAll()


		# millis = int(round(time.time() * 1000))
		# outputfolder = "output_%s" % str(millis)


		# "Instal tcp_pobe module and dump to file"
		# os.system("rmmod tcp_probe; modprobe tcp_probe;")
		# Popen("cat /proc/net/tcpprobe > %s/tcp_probe.txt" % outputfolder, shell=True)


		#Run our experiment

		run_experiment(net, topo, 1, "output_mptcp_" + topo_name + "_" + str(exp_no))


		#Here just use p.terminate() to stop the process when you are done
		net.stop()

		# os.system("killall -9 cat; rmmod tcp_probe &>/dev/null;")



		p.terminate()
		# Give riplpox time to shut down.
		sleep(10)
		os.system('stty echo')

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "Usage: <program> <topo-type>"
        sys.exit(1)
    main(topo_name=sys.argv[1])
