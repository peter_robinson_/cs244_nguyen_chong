/************************************************************************/
/* $Id: MainP.cpp 65 2010-09-08 06:48:36Z yan.qi.asu $                                                                 */
/************************************************************************/

#include <limits>
#include <set>
#include <map>
#include <queue>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include "GraphElements.h"
#include "Graph.h"
#include "DijkstraShortestPathAlg.h"
#include "YenTopKShortestPathsAlg.h"
#include <sstream>

using namespace std;

/*
void testDijkstraGraph()
{
	Graph* my_graph_pt = new Graph("data/danYen");
	DijkstraShortestPathAlg shortest_path_alg(my_graph_pt);
	BasePath* result =
		shortest_path_alg.get_shortest_path(
			my_graph_pt->get_vertex(46), my_graph_pt->get_vertex(13));
	result->PrintOut(cout);
}
*/

void testYenAlg(char *graph_data, int node_1, int node_2, int num_paths)
{
	//Graph my_graph("data/jelly_1");
	Graph my_graph(graph_data);

	YenTopKShortestPathsAlg yenAlg(my_graph, my_graph.get_vertex(node_1),
		my_graph.get_vertex(node_2));

	int i=0;
	while (yenAlg.has_next() && i < num_paths)
	{
		++i;
		yenAlg.next()->PrintOut(cout);
	}

// 	System.out.println("Result # :"+i);
// 	System.out.println("Candidate # :"+yenAlg.get_cadidate_size());
// 	System.out.println("All generated : "+yenAlg.get_generated_path_size());

}

int main(int argc, char **argv)
{
	//cout << "Welcome to the real world!" << endl;
	if (argc < 4 ) {
		cout << "Format: program 'graph data here' node1 node2 [k-paths]" << endl;
		return 1;
	}
//	testDijkstraGraph();
//
	stringstream tokenizer;
	int node_1, node_2, num_paths = 4;

	char *file = argv[1];
	//tokenizer << argv[1];
	//tokenizer >> file;
	//tokenizer.clear();
	

	tokenizer << argv[2];
	tokenizer >> node_1;
	tokenizer.clear();

	tokenizer << argv[3];
	tokenizer >> node_2;
	tokenizer.clear();

	// Limit the number of paths output
	if (argc == 5) {
		tokenizer << argv[4];
		tokenizer >> num_paths;
		tokenizer.clear();
	}

	testYenAlg(file, node_1, node_2, num_paths);
}
