#!/usr/bin/env python
'''@package dctopo

Data center network topology creation and drawing.

@author Brandon Heller (brandonh@stanford.edu)

This package includes code to create and draw networks with a regular,
repeated structure.  The main class is StructuredTopo, which augments the
standard Mininet Topo object with layer metadata plus convenience functions to
enumerate up, down, and layer edges.
'''

from mininet.topo import Topo
from random import choice, seed


PORT_BASE = 1  # starting index for OpenFlow switch ports


class NodeID(object):
    '''Topo node identifier.'''

    def __init__(self, dpid = None):
        '''Init.

        @param dpid dpid
        '''
        # DPID-compatible hashable identifier: opaque 64-bit unsigned int
        self.dpid = dpid

    def __str__(self):
        '''String conversion.

        @return str dpid as string
        '''
        return str(self.dpid)

    def name_str(self):
        '''Name conversion.

        @return name name as string
        '''
        return str(self.dpid)

    def ip_str(self):
        '''Name conversion.

        @return ip ip as string
        '''
        hi = (self.dpid & 0xff0000) >> 16
        mid = (self.dpid & 0xff00) >> 8
        lo = self.dpid & 0xff
        return "10.%i.%i.%i" % (hi, mid, lo)


class StructuredNodeSpec(object):
    '''Layer-specific vertex metadata for a StructuredTopo graph.'''

    def __init__(self, up_total, down_total, up_speed, down_speed,
                 type_str = None):
        '''Init.

        @param up_total number of up links
        @param down_total number of down links
        @param up_speed speed in Gbps of up links
        @param down_speed speed in Gbps of down links
        @param type_str string; model of switch or server
        '''
        self.up_total = up_total
        self.down_total = down_total
        self.up_speed = up_speed
        self.down_speed = down_speed
        self.type_str = type_str


class StructuredEdgeSpec(object):
    '''Static edge metadata for a StructuredTopo graph.'''

    def __init__(self, speed = 1.0):
        '''Init.

        @param speed bandwidth in Gbps
        '''
        self.speed = speed


class StructuredTopo(Topo):
    '''Data center network representation for structured multi-trees.'''

    def __init__(self, node_specs, edge_specs):
        '''Create StructuredTopo object.

        @param node_specs list of StructuredNodeSpec objects, one per layer
        @param edge_specs list of StructuredEdgeSpec objects for down-links,
            one per layer
        '''
        super(StructuredTopo, self).__init__()
        self.node_specs = node_specs
        self.edge_specs = edge_specs

    def def_nopts(self, layer):
        '''Return default dict for a structured topo.

        @param layer layer of node
        @return d dict with layer key/val pair, plus anything else (later)
        '''
        return {'layer': layer}

    def layer(self, name):
        '''Return layer of a node

        @param name name of switch
        @return layer layer of switch
        '''
        return self.node_info[name]['layer']

    def isPortUp(self, port):
        ''' Returns whether port is facing up or down

        @param port port number
        @return portUp boolean is port facing up?
        '''
        return port % 2 == PORT_BASE

    def layer_nodes(self, layer):
        '''Return nodes at a provided layer.

        @param layer layer
        @return names list of names
        '''
        def is_layer(n):
            '''Returns true if node is at layer.'''
            return self.layer(n) == layer

        nodes = [n for n in self.g.nodes() if is_layer(n)]
        return nodes

    def up_nodes(self, name):
        '''Return edges one layer higher (closer to core).

        @param name name

        @return names list of names
        '''
        layer = self.layer(name) - 1
        nodes = [n for n in self.g[name] if self.layer(n) == layer]
        return nodes

    def down_nodes(self, name):
        '''Return edges one layer higher (closer to hosts).

        @param name name
        @return names list of names
        '''
        layer = self.layer(name) + 1
        nodes = [n for n in self.g[name] if self.layer(n) == layer]
        return nodes

    def up_edges(self, name):
        '''Return edges one layer higher (closer to core).

        @param name name
        @return up_edges list of name pairs
        '''
        edges = [(name, n) for n in self.up_nodes(name)]
        return edges

    def down_edges(self, name):
        '''Return edges one layer lower (closer to hosts).

        @param name name
        @return down_edges list of name pairs
        '''
        edges = [(name, n) for n in self.down_nodes(name)]
        return edges

#    def draw(self, filename = None, edge_width = 1, node_size = 1,
#             node_color = 'g', edge_color = 'b'):
#        '''Generate image of RipL network.
#
#        @param filename filename w/ext to write; if None, show topo on screen
#        @param edge_width edge width in pixels
#        @param node_size node size in pixels
#        @param node_color node color (ex 'b' , 'green', or '#0000ff')
#        @param edge_color edge color
#        '''
#        import matplotlib.pyplot as plt
#
#        pos = {} # pos[vertex] = (x, y), where x, y in [0, 1]
#        for layer in range(len(self.node_specs)):
#            v_boxes = len(self.node_specs)
#            height = 1 - ((layer + 0.5) / v_boxes)
#
#            layer_nodes = sorted(self.layer_nodes(layer, False))
#            h_boxes = len(layer_nodes)
#            for j, dpid in enumerate(layer_nodes):
#                pos[dpid] = ((j + 0.5) / h_boxes, height)
#
#        fig = plt.figure(1)
#        fig.clf()
#        ax = fig.add_axes([0, 0, 1, 1], frameon = False)
#
#        draw_networkx_nodes(self.g, pos, ax = ax, node_size = node_size,
#                               node_color = node_color, with_labels = False)
#        # Work around networkx bug; does not handle color arrays properly
#        for edge in self.edges(False):
#            draw_networkx_edges(self.g, pos, [edge], ax = ax,
#                                edge_color = edge_color, width = edge_width)
#
#        # Work around networkx modifying axis limits
#        ax.set_xlim(0, 1.0)
#        ax.set_ylim(0, 1.0)
#        ax.set_axis_off()
#
#        if filename:
#            plt.savefig(filename)
#        else:
#            plt.show()


class FatTreeTopo(StructuredTopo):
    '''Three-layer homogeneous Fat Tree.

    From "A scalable, commodity data center network architecture, M. Fares et
    al. SIGCOMM 2008."
    '''
    LAYER_CORE = 0
    LAYER_AGG = 1
    LAYER_EDGE = 2
    LAYER_HOST = 3

    class FatTreeNodeID(NodeID):
        '''Fat Tree-specific node.'''

        def __init__(self, pod = 0, sw = 0, host = 0, dpid = None, name = None):
            '''Create FatTreeNodeID object from custom params.

            Either (pod, sw, host) or dpid must be passed in.

            @param pod pod ID
            @param sw switch ID
            @param host host ID
            @param dpid optional dpid
            @param name optional name
            '''
            if dpid:
                self.pod = (dpid & 0xff0000) >> 16
                self.sw = (dpid & 0xff00) >> 8
                self.host = (dpid & 0xff)
                self.dpid = dpid
            elif name:
                pod, sw, host = [int(s) for s in name.split('_')]
                self.pod = pod
                self.sw = sw
                self.host = host
                self.dpid = (pod << 16) + (sw << 8) + host
            else:
                self.pod = pod
                self.sw = sw
                self.host = host
                self.dpid = (pod << 16) + (sw << 8) + host

        def __str__(self):
            return "(%i, %i, %i)" % (self.pod, self.sw, self.host)

        def name_str(self):
            '''Return name string'''
            return "%i_%i_%i" % (self.pod, self.sw, self.host)

        def mac_str(self):
            '''Return MAC string'''
            return "00:00:00:%02x:%02x:%02x" % (self.pod, self.sw, self.host)

        def ip_str(self):
            '''Return IP string'''
            return "10.%i.%i.%i" % (self.pod, self.sw, self.host)
    """
    def _add_port(self, src, dst):
        '''Generate port mapping for new edge.

        Since Node IDs are assumed hierarchical and unique, we don't need to
        maintain a port mapping.  Instead, compute port values directly from
        node IDs and topology knowledge, statelessly, for calls to self.port.

        @param src source switch DPID
        @param dst destination switch DPID
        '''
        pass
    """
    def def_nopts(self, layer, name = None):
        '''Return default dict for a FatTree topo.

        @param layer layer of node
        @param name name of node
        @return d dict with layer key/val pair, plus anything else (later)
        '''
        d = {'layer': layer}
        if name:
            id = self.id_gen(name = name)
            # For hosts only, set the IP
            if layer == self.LAYER_HOST:
              d.update({'ip': id.ip_str()})
              d.update({'mac': id.mac_str()})
            d.update({'dpid': "%016x" % id.dpid})
        return d


    def __init__(self, k = 4, speed = 1.0):
        '''Init.

        @param k switch degree
        @param speed bandwidth in Gbps
        '''
        core = StructuredNodeSpec(0, k, None, speed, type_str = 'core')
        agg = StructuredNodeSpec(k / 2, k / 2, speed, speed, type_str = 'agg')
        edge = StructuredNodeSpec(k / 2, k / 2, speed, speed,
                                  type_str = 'edge')
        host = StructuredNodeSpec(1, 0, speed, None, type_str = 'host')
        node_specs = [core, agg, edge, host]

        # print "Edge spec: " + str(speed) + "Gbps"    
        edge_specs = [StructuredEdgeSpec(speed)] * 3
        super(FatTreeTopo, self).__init__(node_specs, edge_specs)

        self.k = k
        self.id_gen = FatTreeTopo.FatTreeNodeID
        self.numPods = k
        self.aggPerPod = k / 2

        pods = range(0, k)
        core_sws = range(1, k / 2 + 1)
        agg_sws = range(k / 2, k)
        edge_sws = range(0, k / 2)
        hosts = range(2, k / 2 + 2)

        # speed_mbps = speed * 1000;
        # link_opts = {'bw':speed_mbps}
        # print "Speed: " + str(speed_mbps) + "Mbps"
        for p in pods:
            for e in edge_sws:
                edge_id = self.id_gen(p, e, 1).name_str()
                edge_opts = self.def_nopts(self.LAYER_EDGE, edge_id)
                self.add_switch(edge_id, **edge_opts)

                for h in hosts:
                    host_id = self.id_gen(p, e, h).name_str()
                    host_opts = self.def_nopts(self.LAYER_HOST, host_id)
                    self.add_host(host_id, **host_opts)

            
                    self.add_link(host_id, edge_id)

                for a in agg_sws:
                    agg_id = self.id_gen(p, a, 1).name_str()
                    agg_opts = self.def_nopts(self.LAYER_AGG, agg_id)
                    self.add_switch(agg_id, **agg_opts)
                    self.add_link(edge_id, agg_id)

            for a in agg_sws:
                agg_id = self.id_gen(p, a, 1).name_str()
                c_index = a - k / 2 + 1
                for c in core_sws:
                    core_id = self.id_gen(k, c_index, c).name_str()
                    core_opts = self.def_nopts(self.LAYER_CORE, core_id)
                    self.add_switch(core_id, **core_opts)
                    self.add_link(core_id, agg_id)


    def port(self, src, dst):
        '''Get port number (optional)

        Note that the topological significance of DPIDs in FatTreeTopo enables
        this function to be implemented statelessly.

        @param src source switch DPID
        @param dst destination switch DPID
        @return tuple (src_port, dst_port):
            src_port: port on source switch leading to the destination switch
            dst_port: port on destination switch leading to the source switch
        '''
        src_layer = self.layer(src)
        dst_layer = self.layer(dst)

        src_id = self.id_gen(name = src)
        dst_id = self.id_gen(name = dst)

        LAYER_CORE = 0
        LAYER_AGG = 1
        LAYER_EDGE = 2
        LAYER_HOST = 3

        if src_layer == LAYER_HOST and dst_layer == LAYER_EDGE:
            src_port = 0
            dst_port = (src_id.host - 2) * 2 + 1
        elif src_layer == LAYER_EDGE and dst_layer == LAYER_CORE:
            src_port = (dst_id.sw - 2) * 2
            dst_port = src_id.pod
        elif src_layer == LAYER_EDGE and dst_layer == LAYER_AGG:
            src_port = (dst_id.sw - self.k / 2) * 2
            dst_port = src_id.sw * 2 + 1
        elif src_layer == LAYER_AGG and dst_layer == LAYER_CORE:
            src_port = (dst_id.host - 1) * 2
            dst_port = src_id.pod
        elif src_layer == LAYER_CORE and dst_layer == LAYER_AGG:
            src_port = dst_id.pod
            dst_port = (src_id.host - 1) * 2
        elif src_layer == LAYER_AGG and dst_layer == LAYER_EDGE:
            src_port = dst_id.sw * 2 + 1
            dst_port = (src_id.sw - self.k / 2) * 2
        elif src_layer == LAYER_CORE and dst_layer == LAYER_EDGE:
            src_port = dst_id.pod
            dst_port = (src_id.sw - 2) * 2
        elif src_layer == LAYER_EDGE and dst_layer == LAYER_HOST:
            src_port = (dst_id.host - 2) * 2 + 1
            dst_port = 0
        else:
            raise Exception("Could not find port leading to given dst switch")

        # Shift by one; as of v0.9, OpenFlow ports are 1-indexed.
        if src_layer != LAYER_HOST:
            src_port += 1
        if dst_layer != LAYER_HOST:
            dst_port += 1

        return (src_port, dst_port)





class JellyFishTopo(StructuredTopo):
    '''
    JellyFish random topology.

    From "Jellyfish: Networking Data Centers Randomly, A. Singla et al. NSDI 2012"

    '''

    LAYER_EDGE = 0
    LAYER_HOST = 1

    class JellyFishNodeID(NodeID):

   
        def __init__(self, sw = 0, host = 0, dpid = None, name = None):
            '''Create FatTreeNodeID object from custom params.

            Either (sw, host) or dpid must be passed in.

            @param sw switch ID
            @param host host ID
            @param dpid optional dpid
            @param name optional name
            '''
            if dpid:
                self.sw = (dpid & 0xff00) >> 8
                self.host = (dpid & 0xff)
                self.dpid = dpid
            elif name:
                sw, host = [int(s) for s in name.split('_')]
                self.sw = sw
                self.host = host
                self.dpid = (sw << 8) + host
            else:
                self.sw = sw
                self.host = host
                self.dpid =(sw << 8) + host

        def __str__(self):
            return "(%i, %i)" % (self.sw, self.host)

        def name_str(self):
            '''Return name string'''
            return "%i_%i" % (self.sw, self.host)

        def mac_str(self):
            '''Return MAC string'''
            return "00:00:00:00:%02x:%02x" % (self.sw, self.host)

        def ip_str(self):
            '''Return IP string'''
            return "10.10.%i.%i" % (self.sw, self.host)

    class JellyFishNode(object):
        def __init__(self, node_id, node_type, k):
            self.id_= node_id
            self.type_ = node_type
            self.neighbors_ = []
            self.num_ports_ = k

        def id(self):
            return self.id_

        def type(self):
            return self.type_

        def add_neighbor(self, node):
            self.neighbors_.append(node)

        def remove_neighbor(self, node):
            self.neighbors_.remove(node)

        def neighbors(self):
            return self.neighbors_

        def num_ports(self):
            return self.num_ports_


    class JellyFishEdge(object):
        def __init__(self, one, two):
            self.one_ = one
            self.two_ = two

        def one(self):
            return self.one_

        def two(self):
            return self.two_

    def def_nopts(self, layer, name = None):
        '''Return default dict for a FatTree topo.

        @param layer layer of node
        @param name name of node
        @return d dict with layer key/val pair, plus anything else (later)
        '''
        d = {'layer': layer}
        if name:
            id = self.id_gen(name = name)
            # For hosts only, set the IP
            if layer == self.LAYER_HOST:
              d.update({'ip': id.ip_str()})
              d.update({'mac': id.mac_str()})
            d.update({'dpid': "%016x" % id.dpid})
        return d

    def rewire(self, node1, node2):
        '''Pick random edge'''
        edge = choice(self.jedges)
        switch_one = self.jnodes[edge.one()];
        switch_two = self.jnodes[edge.two()];

        switch_one.remove_neighbor(switch_two.id())
        switch_two.remove_neighbor(switch_one.id())

        node1.add_neighbor(switch_one.id())
        node2.add_neighbor(switch_two.id())

        switch_one.add_neighbor(node1.id())
        switch_two.add_neighbor(node2.id())

        self.jedges.remove(edge)
        self.jedges.append(JellyFishTopo.JellyFishEdge(switch_one.id(), node1.id()))
        self.jedges.append(JellyFishTopo.JellyFishEdge(switch_two.id(), node2.id()))

        if(len(node1.neighbors()) <= 0):
            del available_nodes[node1.id()]
        if(node1.id() != node2.id() and len(node2.neighbors()) <= 0):
            del available_nodes[node2.id()]


    def __init__(self, n=8, k=4, h=8, use_seed=0, speed=1.0):
        ''' Init

        @param n number of switches
        @param k switch degree
        @param h number of hosts
        '''

	''' We keep a global view of things for routing later '''
	self.graph_data = "%i\n" % n
        self.n_ = n
        self.k_ = k
        self.h_ = h

        assert(n >= 0)
        assert(k >= 0)
        assert(h >= 0)

        seed(use_seed)

        n_down_link = h/n #Hosts per switch
        n_up_link = k - n_down_link
        edge = StructuredNodeSpec(n_up_link, n_down_link, speed, speed,
                                  type_str = 'edge')
        host = StructuredNodeSpec(1, 0, speed, None, type_str = 'host')
        node_specs = [edge, host]
        edge_specs = [StructuredEdgeSpec(speed)] * 3
        super(JellyFishTopo, self).__init__(node_specs, edge_specs)

	print "Adding id_gen"
        self.id_gen = JellyFishTopo.JellyFishNodeID
    

        self.jnodes = {}
        self.jedges = []

        '''Set up switches and hosts'''
        for i in range(n):
            switch_name = self.id_gen(i+1,0).name_str()
            switch_opts = self.def_nopts(self.LAYER_EDGE, switch_name)
            self.add_switch(switch_name, **switch_opts)
            print "Added switch: " + switch_name

            switch_node = JellyFishTopo.JellyFishNode(switch_name, "switch", k);
            self.jnodes[switch_name] = switch_node

        for j in range(h):
            switch_number = j % n
            host_number = int(j/n) + 1
            host_name = self.id_gen(switch_number + 1, host_number).name_str()
            switch_name = self.id_gen(switch_number + 1, 0).name_str()
            host_opts = self.def_nopts(self.LAYER_HOST, host_name)
            self.add_host(host_name, **host_opts)
            self.add_link(host_name, switch_name)

            print "Added host: " + host_name
        
            switch_node = self.jnodes[switch_name]
            host_node = JellyFishTopo.JellyFishNode(host_name, "host", 1);


            switch_node.add_neighbor(host_name)
            host_node.add_neighbor(switch_name)

            self.jnodes[host_name] = host_node

            



        '''Connect switches using Jellyfish algorithm

            while(no available switches with free ports):
                randomly choose 2 switches with available ports to connect
                if (only pair available (x,y) is on the same switch):
                    disconnect some other pair (p1, p2)
                    connect (p1, x) and (p2, y)
        '''

        '''Add switches to bag of available switches'''
        available_nodes = {}
        for i in range(n):
            switch_id = self.id_gen(i+1, 0).name_str()
            node = self.jnodes[switch_id]
            if(len(node.neighbors()) < node.num_ports()):
                available_nodes[switch_id] = node
            

        iter = 0
        while(len(available_nodes) >= 2):
            iter += 1
            # print "Iteration " + str(iter)
            '''Pick 2 switches randomly'''
            switch1 = available_nodes[choice(available_nodes.keys())]
            switch2 = available_nodes[choice(available_nodes.keys())]
            numRetries = 0
            MAX_RETRIES = 10
            while((switch1.id() == switch2.id() or switch2.id() in switch1.neighbors()) and numRetries < MAX_RETRIES):
                # print "Retrying because switch1 id: " + str(switch1.id()) + " switch2 id: " + str(switch2.id()) + " and neighbors? " + str(switch2.id() in switch1.neighbors())
                switch2 = available_nodes[choice(available_nodes.keys())]
                numRetries += 1

            if(numRetries == MAX_RETRIES):
                break

            '''Connect the switches'''
            edge = JellyFishTopo.JellyFishEdge(switch1.id(), switch2.id());
            self.jedges.append(edge)

            # print "added edge between " + switch1.id() + " and " + switch2.id() 
            switch1.add_neighbor(switch2.id())
            switch2.add_neighbor(switch1.id())

            if(len(switch1.neighbors()) >= switch1.num_ports()):
                del available_nodes[switch1.id()]

            if(len(switch2.neighbors()) >= switch2.num_ports()):
                del available_nodes[switch2.id()]

            '''If there is only 1 switch left, and it has 2 available ports
            '''
            if(len(available_nodes) == 1):
                node = available_nodes.items()[0][1];
                if(node.num_ports() - len(node.neighbors()) >= 2):
                    self.rewire(node, node);
                else:
                    available_nodes.remove(node.id())

            '''If there are 2 switches left with free ports, but they're already connected
                to eachother
            '''
            if(len(available_nodes) == 2):
                node1 = available_nodes.items()[0][1];
                node2 = available_nodes.items()[1][1];
                if(node1.id() in node2.neighbors()):
                    self.rewire(node1, node2)



        '''Create actual links'''
        for edge in self.jedges:
            link_opts = {}
            self.add_link(edge.one(), edge.two(), **link_opts)

	    ''' Add add them to the global graph view '''
	    ''' We have to hack it by taking off the last two chars "_0" '''
            #print "Adding edge: %s %s" %(edge_one, edge_two)

            edge_one = edge.one().split("_")[0]
            edge_two = edge.two().split("_")[0]

            graph_edge = "%s %s 1\n" % (edge_one, edge_two) 
	    self.graph_data += graph_edge 
            graph_edge = "%s %s 1\n" % (edge_two, edge_one) 
	    self.graph_data += graph_edge 
        # print "** Actual Links up"
        # print self.graph_data
  
